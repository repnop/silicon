use crate::token::*;
use itertools::Itertools;

#[derive(Debug, Clone, PartialEq)]
pub enum LexError {
    InvalidNum,
    Err,
}

pub trait Lexer {
    fn in_first_set(&self, c: char) -> bool;
    fn lex(&self, stream: &mut TokenStream) -> Result<Token, LexError>;
}

pub struct IdentifierLexer;

impl Lexer for IdentifierLexer {
    fn in_first_set(&self, c: char) -> bool {
        !c.is_whitespace() && c != '(' && c != ')' && c != '\'' && !c.is_digit(10)
    }

    fn lex(&self, stream: &mut TokenStream) -> Result<Token, LexError> {
        let v = stream
            .peeking_take_while(|c| !c.is_whitespace() && c != &'(' && c != &')' && c != &'\'')
            .collect::<String>();

        if v.to_lowercase() == "true" {
            Ok(Token::True)
        } else {
            Ok(Token::Ident(v))
        }
    }
}

pub struct NumberLexer;

impl Lexer for NumberLexer {
    fn in_first_set(&self, c: char) -> bool {
        c.is_digit(10)
    }

    fn lex(&self, stream: &mut TokenStream) -> Result<Token, LexError> {
        let v = stream
            .peeking_take_while(|c| c.is_digit(10) || c == &'.')
            .collect::<String>();

        Ok(Token::Num(v.parse().map_err(|_| LexError::InvalidNum)?))
    }
}

pub struct ParenLexer;

impl Lexer for ParenLexer {
    fn in_first_set(&self, c: char) -> bool {
        c == '(' || c == ')'
    }

    fn lex(&self, stream: &mut TokenStream) -> Result<Token, LexError> {
        let c = stream.next().unwrap();

        if c == '(' {
            Ok(Token::LParen)
        } else if c == ')' {
            Ok(Token::RParen)
        } else {
            return Err(LexError::Err);
        }
    }
}

pub struct QuoteLexer;

impl Lexer for QuoteLexer {
    fn in_first_set(&self, c: char) -> bool {
        c == '\''
    }

    fn lex(&self, stream: &mut TokenStream) -> Result<Token, LexError> {
        let c = stream.next().unwrap();

        if c == '\'' {
            Ok(Token::Quote)
        } else {
            return Err(LexError::Err);
        }
    }
}
