use crate::ast::Atom;
use crate::token::*;
use std::collections::VecDeque;

pub fn parse_tokens(tokens: &mut VecDeque<Token>) -> Atom {
    let tkn = tokens.pop_front();

    if let Some(tkn) = tkn {
        if let Token::Num(n) = tkn {
            Atom::Num(n)
        } else if let Token::Ident(s) = tkn {
            Atom::Ident(s)
        } else if let Token::Quote = tkn {
            let mut lst = VecDeque::new();
            lst.push_back(Atom::Ident("quote".into()));
            lst.push_back(parse_tokens(tokens));

            Atom::List(lst)
        } else if let Token::LParen = tkn {
            let mut list = VecDeque::new();

            while let Some(tk) = tokens.front() {
                if let Token::RParen = tk {
                    tokens.pop_front();
                    break;
                } else {
                    list.push_back(parse_tokens(tokens))
                }
            }

            if list.len() > 0 {
                Atom::List(list)
            } else {
                Atom::Nil
            }
        } else {
            Atom::Nil
        }
    } else {
        Atom::Nil
    }
}
