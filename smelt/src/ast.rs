use std::collections::VecDeque;

#[derive(Debug, Clone, PartialEq)]
pub enum Atom {
    Num(f64),
    Ident(String),
    List(VecDeque<Atom>),
    Nil,
    True,
}

impl Atom {
    pub fn get_ident(self) -> String {
        if let Atom::Ident(s) = self {
            s
        } else {
            panic!()
        }
    }

    pub fn get_num(self) -> f64 {
        if let Atom::Num(s) = self {
            s
        } else {
            panic!()
        }
    }

    pub fn get_list(self) -> VecDeque<Atom> {
        if let Atom::List(s) = self {
            s
        } else {
            panic!()
        }
    }
}

impl std::fmt::Display for Atom {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Atom::Num(n) => write!(f, "{}", n),
            Atom::Ident(i) => write!(f, "{}", i),
            Atom::List(l) => {
                write!(f, "(");
                write!(f, "{}", l[0]);
                for atom in l.iter().skip(1) {
                    write!(f, " {}", atom);
                }
                write!(f, ")")
            }
            Atom::Nil => write!(f, "nil"),
            Atom::True => write!(f, "true"),
        }
    }
}
