pub type TokenStream<'a> = std::iter::Peekable<std::str::Chars<'a>>;

#[derive(Debug, Clone, PartialEq)]
pub enum Token {
    Num(f64),
    Ident(String),
    String(String),
    LParen,
    RParen,
    True,
    Quote,
}

impl Token {
    pub fn is_num(&self) -> bool {
        match self {
            Token::Num(_) => true,
            _ => false,
        }
    }

    pub fn is_ident(&self) -> bool {
        match self {
            Token::Ident(_) => true,
            _ => false,
        }
    }

    pub fn is_string(&self) -> bool {
        match self {
            Token::String(_) => true,
            _ => false,
        }
    }
}
