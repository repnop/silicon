pub mod ast;
mod lex;
mod parse;
mod token;

use self::ast::Atom;
use self::lex::*;
use self::token::Token;
use std::collections::VecDeque;

pub fn tokenize(source: &str) -> Result<VecDeque<Token>, LexError> {
    let mut tokens = VecDeque::with_capacity(source.len());

    let mut chars = source.chars().peekable();

    let lexers: &[&dyn Lexer] = &[
        &ParenLexer {},
        &NumberLexer {},
        &QuoteLexer {},
        &IdentifierLexer {},
    ];

    'outer: while let Some(&c) = chars.peek() {
        if c.is_whitespace() {
            chars.next().unwrap();
            continue;
        }

        for lexer in lexers {
            if lexer.in_first_set(c) {
                tokens.push_back(lexer.lex(&mut chars)?);
                continue 'outer;
            }
        }

        return Err(LexError::Err);
    }

    Ok(tokens)
}

pub fn parse(mut tokens: VecDeque<Token>) -> Atom {
    parse::parse_tokens(&mut tokens)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_tokenize() {
        let source = "(foo 1 2 3 bar)";

        assert_eq!(
            Ok(vec![
                Token::LParen,
                Token::Ident("foo".into()),
                Token::Num(1.0),
                Token::Num(2.0),
                Token::Num(3.0),
                Token::Ident("bar".into()),
                Token::RParen,
            ]),
            tokenize(&source).map(|t| t.into())
        );
    }
}
