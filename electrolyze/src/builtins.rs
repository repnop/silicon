use crate::context::ExecutionContext;
use crate::fns::UserFn;
use smelt::ast::Atom;

fn preevaluate_args(ctx: &mut ExecutionContext, atoms: Vec<Atom>) -> Vec<Atom> {
    atoms.into_iter().map(|a| ctx.evaluate(a)).collect()
}

pub fn quote(_: &mut ExecutionContext, mut atoms: Vec<Atom>) -> Atom {
    atoms.remove(0)
}

pub fn car(ctx: &mut ExecutionContext, mut atoms: Vec<Atom>) -> Atom {
    atoms = preevaluate_args(ctx, atoms);

    if let Atom::List(mut lst) = atoms.remove(0) {
        lst.pop_front().or(Some(Atom::Nil)).unwrap()
    } else {
        unimplemented!()
    }
}

pub fn cdr(ctx: &mut ExecutionContext, mut atoms: Vec<Atom>) -> Atom {
    atoms = preevaluate_args(ctx, atoms);

    if let Atom::List(mut lst) = atoms.remove(0) {
        if lst.len() > 1 {
            lst.drain(..1);

            Atom::List(lst)
        } else {
            Atom::Nil
        }
    } else {
        unimplemented!()
    }
}

pub fn cons(ctx: &mut ExecutionContext, mut atoms: Vec<Atom>) -> Atom {
    atoms = preevaluate_args(ctx, atoms);

    if let Atom::List(mut lst2) = atoms.remove(1) {
        lst2.insert(0, atoms.remove(0));

        Atom::List(lst2)
    } else if let Atom::Nil = atoms.remove(1) {
        Atom::List(vec![atoms.remove(0)].into())
    } else {
        unimplemented!()
    }
}

pub fn eq(ctx: &mut ExecutionContext, mut atoms: Vec<Atom>) -> Atom {
    atoms = preevaluate_args(ctx, atoms);

    if atoms[0] == atoms[1] {
        Atom::True
    } else {
        Atom::Nil
    }
}

pub fn neq(ctx: &mut ExecutionContext, mut atoms: Vec<Atom>) -> Atom {
    atoms = preevaluate_args(ctx, atoms);

    if atoms[0] != atoms[1] {
        Atom::True
    } else {
        Atom::Nil
    }
}

pub fn lt(ctx: &mut ExecutionContext, mut atoms: Vec<Atom>) -> Atom {
    atoms = preevaluate_args(ctx, atoms);

    match (atoms.remove(0), atoms.remove(0)) {
        (Atom::Num(n1), Atom::Num(n2)) => if n1 < n2 {
            Atom::True
        } else {
            Atom::Nil
        },
        (Atom::Ident(n1), Atom::Ident(n2)) => if n1 < n2 {
            Atom::True
        } else {
            Atom::Nil
        },
        (Atom::List(_), Atom::List(_)) => unimplemented!(),
        (_, _) => {
            println!("Can't compare values of different types");
            Atom::Nil
        }
    }
}

pub fn gt(ctx: &mut ExecutionContext, mut atoms: Vec<Atom>) -> Atom {
    atoms = preevaluate_args(ctx, atoms);

    match (atoms.remove(0), atoms.remove(0)) {
        (Atom::Num(n1), Atom::Num(n2)) => if n1 > n2 {
            Atom::True
        } else {
            Atom::Nil
        },
        (Atom::Ident(n1), Atom::Ident(n2)) => if n1 > n2 {
            Atom::True
        } else {
            Atom::Nil
        },
        (Atom::List(_), Atom::List(_)) => unimplemented!(),
        (_, _) => {
            println!("Can't compare values of different types");
            Atom::Nil
        }
    }
}

pub fn lteq(ctx: &mut ExecutionContext, mut atoms: Vec<Atom>) -> Atom {
    atoms = preevaluate_args(ctx, atoms);

    match (atoms.remove(0), atoms.remove(0)) {
        (Atom::Num(n1), Atom::Num(n2)) => if n1 <= n2 {
            Atom::True
        } else {
            Atom::Nil
        },
        (Atom::Ident(n1), Atom::Ident(n2)) => if n1 <= n2 {
            Atom::True
        } else {
            Atom::Nil
        },
        (Atom::List(_), Atom::List(_)) => unimplemented!(),
        (_, _) => {
            println!("Can't compare values of different types");
            Atom::Nil
        }
    }
}

pub fn gteq(ctx: &mut ExecutionContext, mut atoms: Vec<Atom>) -> Atom {
    atoms = preevaluate_args(ctx, atoms);

    match (atoms.remove(0), atoms.remove(0)) {
        (Atom::Num(n1), Atom::Num(n2)) => if n1 >= n2 {
            Atom::True
        } else {
            Atom::Nil
        },
        (Atom::Ident(n1), Atom::Ident(n2)) => if n1 >= n2 {
            Atom::True
        } else {
            Atom::Nil
        },
        (Atom::List(_), Atom::List(_)) => unimplemented!(),
        (_, _) => {
            println!("Can't compare values of different types");
            Atom::Nil
        }
    }
}

pub fn atom(ctx: &mut ExecutionContext, mut atoms: Vec<Atom>) -> Atom {
    atoms = preevaluate_args(ctx, atoms);

    if let Atom::Nil = atoms.remove(0) {
        Atom::Nil
    } else {
        Atom::True
    }
}

pub fn null(_ctx: &mut ExecutionContext, mut atoms: Vec<Atom>) -> Atom {
    atoms = atoms.into_iter().map(|a| _ctx.evaluate(a)).collect();

    if let Atom::Nil = atom(_ctx, atoms) {
        Atom::True
    } else {
        Atom::Nil
    }
}

pub fn add(ctx: &mut ExecutionContext, atoms: Vec<Atom>) -> Atom {
    Atom::Num(
        atoms
            .into_iter()
            .map(|a| ctx.evaluate(a))
            .map(|a| a.get_num())
            .fold(0.0, |acc, n| acc + n),
    )
}

pub fn sub(ctx: &mut ExecutionContext, atoms: Vec<Atom>) -> Atom {
    let mut res = 0.0;
    let mut first = true;

    atoms
        .into_iter()
        .map(|a| ctx.evaluate(a))
        .map(|a| a.get_num())
        .for_each(|a| {
            if first {
                res = a;
                first = false;
            } else {
                res = res - a
            }
        });

    Atom::Num(res)
}

pub fn mult(ctx: &mut ExecutionContext, atoms: Vec<Atom>) -> Atom {
    let mut res = 1.0;

    atoms
        .into_iter()
        .map(|a| ctx.evaluate(a))
        .map(|a| a.get_num())
        .for_each(|a| res = a * res);

    Atom::Num(res)
}

pub fn div(ctx: &mut ExecutionContext, atoms: Vec<Atom>) -> Atom {
    let mut res = 1.0;
    let mut first = true;

    atoms
        .into_iter()
        .map(|a| ctx.evaluate(a))
        .map(|a| a.get_num())
        .for_each(|a| {
            if first {
                res = a;
                first = false;
            } else {
                res = res / a
            }
        });

    Atom::Num(res)
}

pub fn _if(ctx: &mut ExecutionContext, mut atoms: Vec<Atom>) -> Atom {
    let val = ctx.evaluate(atoms.remove(0));

    if let Atom::Nil = val {
        ctx.evaluate(atoms.remove(1))
    } else {
        ctx.evaluate(atoms.remove(0))
    }
}

pub fn defun(ctx: &mut ExecutionContext, mut atoms: Vec<Atom>) -> Atom {
    let name = atoms.remove(0);
    let args = atoms.remove(0);
    let body = atoms.remove(0);

    let arg_names: Vec<String> = if let Atom::List(lst) = args {
        lst.into_iter().map(|a| a.get_ident()).collect()
    } else {
        unimplemented!()
    };

    ctx.add_user_fn(UserFn::new(
        name.get_ident(),
        arg_names.len(),
        arg_names,
        body,
    ));

    Atom::Nil
}
