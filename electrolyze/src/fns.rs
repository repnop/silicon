use crate::context::ExecutionContext;
use smelt::ast::Atom;

/*#[derive(Debug, Clone, Copy)]
pub enum LispFnError {
    TooManyArguments,
    NotEnoughArguments,
}*/

#[derive(Debug, Clone)]
pub enum LispFn {
    User(UserFn),
    Builtin(BuiltinFn),
}

#[derive(Debug, Clone)]
pub struct UserFn {
    pub name: String,
    pub arg_count: usize,
    pub arg_names: Vec<String>,
    pub body: Atom,
}

impl UserFn {
    pub fn new(name: String, arg_count: usize, arg_names: Vec<String>, body: Atom) -> Self {
        Self {
            name,
            arg_count,
            arg_names,
            body,
        }
    }

    pub fn eval(&self, ctx: &mut ExecutionContext) -> Atom {
        ctx.evaluate(self.body.clone())
    }
}

#[derive(Clone)]
pub struct BuiltinFn {
    pub name: &'static str,
    pub arg_count: usize,
    pub func: fn(&mut ExecutionContext, Vec<Atom>) -> Atom,
}

impl std::fmt::Debug for BuiltinFn {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "BuiltinFn {{ name: {}, arg_count: {}, func: <builtinfn> }}",
            self.name, self.arg_count
        )
    }
}

impl BuiltinFn {
    pub fn new(
        name: &'static str,
        arg_count: usize,
        func: fn(&mut ExecutionContext, Vec<Atom>) -> Atom,
    ) -> Self {
        Self {
            name,
            arg_count,
            func,
        }
    }

    pub fn eval(&self, ctx: &mut ExecutionContext, args: Vec<Atom>) -> Atom {
        (self.func)(ctx, args)
    }
}
