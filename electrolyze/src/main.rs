mod builtins;
mod context;
mod fns;

use self::context::ExecutionContext;
use std::io::{stdin, stdout, BufRead, Write};

fn main() {
    let mut global_context = ExecutionContext::global();

    print!("> ");
    stdout().lock().flush().unwrap();

    for line in stdin().lock().lines() {
        let line = line.unwrap();

        if line == "quit" {
            break;
        }

        let tkns = smelt::tokenize(&line).unwrap();
        let atoms = smelt::parse(tkns);

        println!("{}", global_context.evaluate(atoms));

        print!("> ");
        stdout().lock().flush().unwrap();
    }
}
