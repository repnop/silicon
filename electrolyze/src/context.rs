use crate::fns::*;
use smelt::ast::Atom;
use std::collections::HashMap;

pub struct ExecutionContext<'ctx, 'parent: 'ctx> {
    parent_context: Option<&'ctx ExecutionContext<'parent, 'parent>>,
    bound_symbols: HashMap<String, Atom>,
    functions: HashMap<String, LispFn>,
}

impl<'ctx, 'parent: 'ctx> ExecutionContext<'ctx, 'parent> {
    pub fn global() -> ExecutionContext<'ctx, 'parent> {
        let mut functions = HashMap::new();

        functions.insert(
            "quote".into(),
            LispFn::Builtin(BuiltinFn::new("quote", 1, crate::builtins::quote)),
        );

        functions.insert(
            "car".into(),
            LispFn::Builtin(BuiltinFn::new("car", 1, crate::builtins::car)),
        );

        functions.insert(
            "cdr".into(),
            LispFn::Builtin(BuiltinFn::new("cdr", 1, crate::builtins::cdr)),
        );

        functions.insert(
            "cons".into(),
            LispFn::Builtin(BuiltinFn::new("cons", 1, crate::builtins::cons)),
        );

        functions.insert(
            "eq".into(),
            LispFn::Builtin(BuiltinFn::new("eq", 1, crate::builtins::eq)),
        );

        functions.insert(
            "neq".into(),
            LispFn::Builtin(BuiltinFn::new("neq", 1, crate::builtins::neq)),
        );

        functions.insert(
            "atom".into(),
            LispFn::Builtin(BuiltinFn::new("atom", 1, crate::builtins::atom)),
        );

        functions.insert(
            "null".into(),
            LispFn::Builtin(BuiltinFn::new("null", 1, crate::builtins::null)),
        );

        functions.insert(
            "defun".into(),
            LispFn::Builtin(BuiltinFn::new("defun", 1, crate::builtins::defun)),
        );

        functions.insert(
            "add".into(),
            LispFn::Builtin(BuiltinFn::new("add", 1, crate::builtins::add)),
        );

        functions.insert(
            "sub".into(),
            LispFn::Builtin(BuiltinFn::new("sub", 1, crate::builtins::sub)),
        );

        functions.insert(
            "mult".into(),
            LispFn::Builtin(BuiltinFn::new("mult", 1, crate::builtins::mult)),
        );

        functions.insert(
            "div".into(),
            LispFn::Builtin(BuiltinFn::new("div", 1, crate::builtins::div)),
        );

        functions.insert(
            "if".into(),
            LispFn::Builtin(BuiltinFn::new("if", 3, crate::builtins::_if)),
        );

        functions.insert(
            "lt".into(),
            LispFn::Builtin(BuiltinFn::new("lt", 2, crate::builtins::lt)),
        );

        functions.insert(
            "gt".into(),
            LispFn::Builtin(BuiltinFn::new("gt", 2, crate::builtins::gt)),
        );

        functions.insert(
            "lteq".into(),
            LispFn::Builtin(BuiltinFn::new("lteq", 2, crate::builtins::lteq)),
        );

        functions.insert(
            "gteq".into(),
            LispFn::Builtin(BuiltinFn::new("gteq", 2, crate::builtins::gteq)),
        );

        Self {
            parent_context: None,
            bound_symbols: HashMap::new(),
            functions,
        }
    }

    pub fn with_parent_context(
        ctx: &'ctx ExecutionContext<'parent, 'parent>,
    ) -> ExecutionContext<'ctx, 'parent> {
        Self {
            parent_context: Some(ctx),
            bound_symbols: HashMap::new(),
            functions: HashMap::new(),
        }
    }

    pub fn resolve_fn(&self, name: &str) -> Option<LispFn> {
        self.functions.get(name).cloned().or_else(|| {
            if let Some(parent) = self.parent_context {
                parent.resolve_fn(name)
            } else {
                None
            }
        })
    }

    pub fn resolve_symbol(&self, symbol: &str) -> Option<Atom> {
        self.bound_symbols.get(symbol).cloned().or_else(|| {
            if let Some(parent) = self.parent_context {
                parent.resolve_symbol(symbol)
            } else {
                None
            }
        })
    }

    pub fn add_bound_symbol(&mut self, symbol: String, value: Atom) {
        self.bound_symbols.insert(symbol, value);
    }

    pub fn add_user_fn(&mut self, userfn: UserFn) {
        self.functions
            .insert(userfn.name.clone(), LispFn::User(userfn));
    }

    pub fn evaluate(&mut self, expr: Atom) -> Atom {
        if let Atom::List(mut lst) = expr {
            if lst.len() == 0 {
                Atom::Nil
            } else {
                if let Atom::Ident(_) = lst.front().unwrap() {
                    let s = lst.pop_front().unwrap().get_ident();

                    let f = self.resolve_fn(&s);

                    if let None = f {
                        self.resolve_symbol(&s).unwrap()
                    } else {
                        let f = f.unwrap();
                        match f {
                            LispFn::User(ufn) => {
                                lst = lst.into_iter().map(|a| self.evaluate(a)).collect();

                                if lst.len() != ufn.arg_count {
                                    println!("Error: Too many or not enough arguments");
                                    return Atom::Nil;
                                }

                                let mut child_ctx = ExecutionContext::with_parent_context(self);

                                for (name, atom) in
                                    ufn.arg_names.iter().cloned().zip(lst.into_iter())
                                {
                                    child_ctx.add_bound_symbol(name, atom);
                                }

                                ufn.eval(&mut child_ctx)
                            }
                            LispFn::Builtin(bfn) => bfn.eval(self, lst.into()),
                        }
                    }
                } else {
                    println!("Error: First item must be a function name");
                    Atom::Nil
                }
            }
        } else if let Atom::Ident(s) = expr {
            self.resolve_symbol(&s)
                .map(|a| a.clone())
                .unwrap_or(Atom::Ident(s))
        } else {
            expr
        }
    }
}
